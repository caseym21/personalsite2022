const nodeMailer = require('nodemailer')
const path = require('path')
const cors = require('cors')
const express = require('express')
const fileUpload = require('express-fileupload')
const server = express()
require("dotenv").config()

//Use Process ENV / Enviroment variables in order to keep stuff like Port # and email/pass private. 
const PORT = process.env.port  || 5000;
const USER = process.env.EMAIL_USER
const PASS = process.env.EMAIL_PASS

const root = require('path').join(__dirname, 'build')
server.use(express.static(root))
server.use(express.json({limit: '3mb'}))
server.use(express.urlencoded({extended: true, limit: '3mb'}))
server.use(fileUpload())
server.use(cors())

server.get('/*', (req,res) =>{
    res.sendFile(path.join(__dirname, 'build', 'index.html'))
})

server.get('/', (req,res) =>{
    res.send("")
})

server.post('/api/contact', (req,res) =>{
    const transporter = nodeMailer.createTransport({
        host: 'smtp.office365.com',
        port: 587,
        secure: false,
        requireTLS: true,
        tls:{
            cipers: 'SSLv3'
        },
        auth: {
            user: USER,
            pass: PASS,
        },
    })
    
    const mailOptions = {
        from: USER,
        to: USER,
        subject: `Email from ${req.body.email}: ${req.body.subject}`,
        text: `Message from ${req.body.sendName}: ${req.body.body}`,
        attachments: [
            {
                filename: req.body.fileName,
                content: req.body.file64,
                encoding: 'base64',
            }
        ],
    }

    transporter.sendMail(mailOptions, (err, info) =>{
        if(err){
            console.log(err)
            res.status(500).send(err)
        } else {
            res.status(200).send('Success')
        }   

    })
})

server.listen(PORT)
