import React from 'react'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'
import Main from './pages/Main'
import Projects from './pages/projects/Projects'
import About from './pages/about/About'
import Contact from './pages/contact/Contact'
import TextEncoderInfo from './pages/info/TextEncoderInfo'
import CryptoConverterInfo from './pages/info/CryptoConverterInfo'
import QuotesInfo from './pages/info/QuotesInfo'
import BeadHangersInfo from './pages/info/BeadHangersInfo'
import CurrencyConverterDemo from './pages/crypto/CurrencyConverterDemo'
// import Snow from './pages/snow/Snow'

const app = () => {
    return (
        <React.Fragment>
            <Router>
                <Switch>
                    <Route exact path='/'>
                        <Main />
                        <Projects />
                    </Route>
                    <Route exact path='/about'>
                        <Main />
                        <About />
                    </Route>
                    <Route exact path='/contact'>
                        <Main />
                        <Contact />
                    </Route>
                    <Route exact path='/TextEncoderInfo'>
                        <Main />
                        <TextEncoderInfo />
                    </Route>
                    <Route exact path='/CryptoConverterInfo'>
                        <Main />
                        <CryptoConverterInfo />
                    </Route>
                    <Route exact path='/BeadHangersInfo'>
                        <Main />
                        <BeadHangersInfo />
                    </Route>
                    <Route exact path='/QuotesInfo'>
                        <Main />
                        <QuotesInfo />
                    </Route>
                    <Route exact path='/CurrencyConverterDemo'>
                        <CurrencyConverterDemo />
                    </Route>
                </Switch>
            </Router>
            {/* <Snow /> */}
        </React.Fragment>
    )
}

export default app
