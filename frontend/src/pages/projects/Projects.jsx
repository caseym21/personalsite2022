import React from 'react'
import './projects.css'

const Projects = () => {
    return (
        <React.Fragment>
        <h3 class="Project-Header">Click on a Project to see More Info!</h3>

        <div className='Container'>
            <a className='Project-Link' href='/TextEncoderInfo'>
                <img src='encode.jpg' alt="./encode.jpg"></img>
            </a>
            <a className='Project-Link' href='/CryptoConverterInfo'>
                <img src='crypto.jpg' alt="./encode.jpg"></img>
            </a>
            <a className='Project-Link' href='/QuotesInfo'>
                <img src='qoc.jpg' alt='qoc.jpg'></img>
            </a>
            <a className='Project-Link' href='/BeadHangersInfo'>
                <img src='beadhanger.png' alt='beadhanger.png'></img>
            </a>
        </div>
        </React.Fragment>
    )
}

export default Projects
