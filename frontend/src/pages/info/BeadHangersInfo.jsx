import React from 'react'
import './info.css'

const BeadHangersInfo = () => {
  return (
    <div className="Info-Container">
      <div className="Flex">
        <a className="Project-Link" href="/BeadHangersDemo">
        <img className="Project-Image" src='beadhanger.png' alt="./beadhanger.png"></img>
        </a>
        <h3>Bead Hanger Calculator - Click the Image to try it Out!</h3>
        <a href="https://gitlab.com/caseym21/beadtool"> View the Source Code! </a>
        <p>
        <b>This</b> is the first real world project that I created at the start of my coding journey. It uses math to automate one of the more tedious parts of my Aunt's bead tapestry work. 
        It took a lot longer than I had expected it to take, but the rewarding feeling after completing it got me hooked on writing code.<br/>
        <b>The</b> tool calculates the number of beads for the centre hangers/supports given the total width of the tapestry and the widths of the side hangers.<br/><br/>
        <b>One</b> problem encountered was the amount of revisions and additional features requested, although this taught me the valuable skill of discussing the details of a project with the client so that less future changes are needed.
        </p>
      </div>

    </div>
  )
}

export default BeadHangersInfo
