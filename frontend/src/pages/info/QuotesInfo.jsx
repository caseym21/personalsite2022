import React from 'react'
import './info.css'

const Quotes = () => {
  return (
    <div className="Info-Container">
      <div className="Flex">
        <a className="Project-Link" href="https://chrome.google.com/webstore/detail/quotes-on-click/pncdcnbgaifdcakpnkipbpelkhkahmil">
        <img className="Project-Image" src='qoc.jpg' alt="./qoc.jpg"></img>
        </a>
        <h3>Quotes on Click - Click the Image to Download the Extension!</h3>
        <h3>My sincerest apologies, but I cannot provide the source code for Quotes on Click due to security reasons. I hope that you can still enjoy the extension given the circumstances!</h3>
        <p>
          <b>This</b> is another progrma that I coded up for my Aunt, as she wanted something that would give her a random quote that she could paste onto the end of her emails.<br/><br/>
          <b>Quotes</b> on Click is an extension that selects a quote from a few different chosen categories, and it then automatically copies the quote and quotee to your Clipboard for easy copy and pasting.<br/>
          You can even choose which category the quotes are chosen from. <br/><br/>
          <b>Quotes</b> on Click is perfect for attaching quotes onto the end of emails, all you have to do is find a quote that you like and then press Control+V to paste it.<br/>
          With the need to look for clever quotes to attach at the end of emails gone, you'll save much more time when it comes to drafting up inspiring and thoughtful emails. All you have to do is choose a category and then click!<br/><br/>
          <b>The</b> Chrome Web Store has restrictions on what extensions can do, so working within those restrictions proved to be a minor challenge. After getting used to React and Node, having to code in vanilla, browser-side Javascript made for an excellent opportunity to review the finer details of ES6, but this is more of a happy accident than anything else.
        </p>
      </div>

    </div>
  )
}

export default Quotes
