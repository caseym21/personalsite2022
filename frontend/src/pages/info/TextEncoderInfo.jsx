import React from 'react'
import './info.css'

const TextEncoderInfo = () => {
  return (
    <div className="Info-Container">
      <div className="Flex">
        <a className="Project-Link" href="/TextEncoderDemo">
          <img className="Project-Image" src='encode.jpg' alt="./encode.jpg"></img>
        </a>
        <h3>Text Encoder - Click the Image to Try it Out!</h3>
        <a href="https://gitlab.com/caseym21/textencoder"> View the Source Code! </a>
        <p>
          <b>This</b> tool allows you to seamlessly encode and decode a message between ASCII text, Binary, ROT13, Decimal, Hexadecimal, and Base64. 
          The conversions are done by leveraging ES6 DOM manipulation handled through events, thus removing the need for React hooks.<br/>
          <b>The</b> Nodejs back end is where the conversions happen after the front end sends it some JSON data. The encoding and decoding are done through functions to make the code more organized.<br/><br/>  
          <b>After</b> having to convert email attachments to a Base64 string in my contact form, I got curious about what exactly Base64 was,
          and that was when I discovered many other forms of cool encoding, so I got to researching how to turn text into various different forms! <br/>
          I still enjoy marvelling at how every ASCII character can be made with only 8 bits of binary.<br/><br/>
          <b>One</b> challenge encountered was the amount of research needed to get some of these encoding methods working, and that's not to mention the work that went into reversing those methods in order to decode what I encoded.
        </p>
      </div>

    </div>
  )
}

export default TextEncoderInfo
