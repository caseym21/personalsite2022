import React from 'react'
import './info.css'

const CryptoConverterInfo = () => {
  return (
    <div className="Info-Container">
      <div className="Flex">
        <a className="Project-Link" href="/CurrencyConverterDemo">
          <img className="Project-Image" src='crypto.jpg' alt="./crypto.jpg"></img>
        </a>
        <h3>Crypto Converter - Click the Image to Try it Out!</h3>
        <a href="https://gitlab.com/caseym21/cryptoconverter"> View the Source Code! </a>
        <p>
          <b>This</b> project uses the Binance, Coingecko, and FreeCurrency APIs to provide conversions from popular fiat currencies to 10 popular cryptocurrencies. 
          It's built using React to take advantage of hooks and re-renders caused by state changes, and it fetches the market data using Axios. <br/><br/>
          <b>One</b> of the things I wanted to accomplish through code was being able to fetch data that is constantly changing. The original idea was fiat exchange rates,
          but that project didn't seem challenging enough, so I decided to add crypto currencies into the mix.<br/> 
          <b>All</b> of that resulted in the tool that stands before you today.<br/>
          <b>An</b> aspect of this tool that I am incredibly proud of is the responsive design on screen widths under 1500px using only one media query and a single CSS property: <b>flex-wrap</b>.<br/><br/>
          <b>One</b> problem encountered was with the Binance API, which only really works with USD, so I had to use FreeCurrencyAPI to convert the USD into various other fiat currencies.<br/> 
          <b>Another</b> problem was the small rate limit for the Cryptonator API, which ended up having to be removed as a provider for prices due to constant HTTP429 errors after a few API requests. 
        </p>
      </div>
    </div>
  )
}

export default CryptoConverterInfo
