import './about.css'

const About = () => {
    return (
        <div>
            <section className='About-Container'>
                <h1>About Me</h1>
                <p> <b>Howdy!</b> I'm Casey, but when it comes to learning new things you can call me Spongebob Squarepants, because I'm a complete sponge. </p>
                <p><b>When</b> I was young I wanted to code video games, but now that I'm older I've realized that I want to make websites, web apps, and everything in between! One of my favourite things about coding is that I now have the ability to bring any ideas in my mind into reality, and this goes for the ideas others have as well.</p>
                <p><b>At</b> the moment, I'm all about using React to create front ends and back ends with Nodejs, and I then deploy the project using Nginx Ubuntu Linux.</p>
                <p><b>Apologies</b> in advance for poor scaling when it comes to 4k monitors and tablets, as I do not own those items and thus can only guess at how the layout looks.</p>
                <h3> Some of My Skills Include: </h3>
                <ul>
                    <li className='About-List'> Front End: HTML/CSS/SASS/Javascript(ES6)/React</li>
                    <li className='About-List'> Back End: NodeJS/Express/PHP/MySQL/MariaDB/Nginx/Lets Encrypt SSL</li>
                    <li className='About-List'> Tools: Git/VSCode/XAMPP/PuTTY/Insomnia/NPM/Composer/Photoshop</li>
                    <li className='About-List'> Misc: REST APIs/Wordpress/WooCommerce/SSH/Lighthouse/WCAG2.0/</li>
                    <li className='About-List'> Misc2: I can cook a pretty mean duck, and my homemade bread is so good that I make 2 loaves every week.</li>
                </ul>
            </section>
        </div>
    )
}

export default About
