import React, { Component } from 'react'
import './main.css'

export default class Main extends Component {

    componentDidMount = () =>{

    }

    render(){
        return (
            <div>
                <header className="header"> 
                    <ul className="nav-list grid">
                        <li className="nav-item">
                            <a href="./About"> About Me </a>
                        </li>
                        <li className="nav-item">
                            <a href="./">Home & Projects</a>
                        </li>
                        <li className="nav-item">
                            <a href="./Contact">Contact Me</a>
                        </li>
                    </ul>
                </header>
            </div>
        )

    }
}

