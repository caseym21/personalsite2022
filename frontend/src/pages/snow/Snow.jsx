﻿import './snow.css'

const Snow = () => {
    return (
        <div>
            <div className="snow layer3 a"></div>
            <div className="snow layer3"></div>
        </div>
    )
}

export default Snow
