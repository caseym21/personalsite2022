import React, { useState } from 'react'
import axios from 'axios'
import './contact.css'

const Contact = () => {
    const [sendName, setSendName] = useState('')
    const [email, setEmail] = useState('')
    const [subject, setSubject] = useState('')
    const [body, setBody] = useState('')
    const [fileName, setFileName] = useState('Upload a File!')
    const [fileSize, setFileSize] = useState(0)
    const [file64, setFile64] = useState('')
    
    function base64(file){
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(){
            setFile64(reader.result.split(',')[1]) 
        }
    }

    return (
        <div className='Text-Container'>
                <h1> Contact Me </h1>
                <p>(For attachments larger than 3Mb, please email me directly at caseydev16@outlook.com)</p>
            <form className='Text-Container-1'>
                <input type='text' id='name' placeholder="Your Name" required value={sendName} onChange={e => {setSendName(e.target.value)}}/>
                <input type='text' id='email' placeholder="Your Email Address" required value={email} onChange={e => {setEmail(e.target.value)}}/> 
                <input type='text' id='subject' placeholder='Email Subject' required value={subject} onChange={e => {setSubject(e.target.value)}}/>
                <textarea id='body' placeholder='Your Message' required value={body} onChange={e => {setBody(e.target.value)}}cols='30' rows='10'></textarea> 
                <br/>
                
                {/* We use the Proxy http://localhost:5000 in Package.json to connect this with backend. */}
                <div class='Submit-Div'>                
                    <label for='file'> Add an Attachment:</label>
                <input type='file' class='File'  onChange={e => {setFileName(e.target.files[0].name); 
                    setFileSize(e.target.files[0].size); base64(e.target.files[0])}}></input>
                </div>
                    <button type='submit' className='submit' onClick={(e) =>{
                        e.preventDefault()
                        let data = {};
                        if(file64)
                        {
                            data = 
                            {
                                sendName: sendName,
                                email: email,
                                subject: subject,
                                body: body,
                                fileName: fileName,
                                fileSize: fileSize,
                                file64: file64,
                            }
                        } 
                        else 
                        {
                            data = 
                            {
                                sendName: sendName,
                                email: email,
                                subject: subject,
                                body: body,
                            }

                        }
                        if(data.sendName && data.email && data.subject && data.body)
                        {
                            if(fileSize > 3000000)
                            {
                                alert('Please send a smaller file, or send it to caseydev16@outlook.com!')
                            } 
                            else 
                            {
                                console.log(data);
                                axios.post('/api/contact', data).then(res=>{   
                                    alert('Email Sent!')
                                    console.log(res.data)
                                }).catch(e =>{
                                    alert('Something went Wrong!')
                                    console.log(e)
                                })
                            }
                        } 
                        else 
                        {
                            alert('Please fill in all fields');
                        }
                    }}>Send Email</button>
            </form>
        </div>
    )
}

export default Contact
